<?php
/**
 * Month View Content Template
 * The content template for the month view of events. This template is also used for
 * the response that is returned on month view ajax requests.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/month/content.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
} ?>

<div id="tribe-events-content" class="tribe-events-month container">	<div class="center"><img src="/wp-content/uploads/2017/03/Dynamatic_web_Colori_PositivoRiduzioneMinima.png"/><p>Durante gli eventi sarà in funzione il bar di Dynamatic.</p></div>
	<div class="center eventi_box"> <p>
 <a  href="#hiddeneventi"  class="popup-with-form"><strong>NON PERDERTI I NOSTRI EVENTI!<BR/> ISCRIVITI ALLA NOSTRA NEWSLETTER!</strong></a>
               <div class="_form_23 white-popup-block zoom-anim-dialog  mfp-hide"  id="hiddeneventi" ></div>
               <script src="https://dynamoscarl.activehosted.com/f/embed.php?id=23" type="text/javascript" charset="utf-8"></script></p></div>


	<!--<br/> oppure<br/><a href="https://docs.google.com/forms/d/1Z_vcjGI-i13Tm3enODdj3_XvYrdSapUbODPZyu_0yW4/viewform"><strong>PROPONI UN EVENTO!</strong></a>--></p></div>
	<!-- Month Title -->
	<?php do_action( 'tribe_events_before_the_title' ) ?>
	<h2 class="tribe-events-page-title"><?php tribe_events_title() ?></h2>
	<?php do_action( 'tribe_events_after_the_title' ) ?>

	<!-- Notices -->
	<?php tribe_the_notices() ?>

	<!-- Month Header -->

	<?php do_action( 'tribe_events_before_header' ) ?>
	<div id="tribe-events-header" <?php tribe_events_the_header_attributes() ?>>

		<!-- Header Navigation -->
		<?php tribe_get_template_part( 'month/nav' ); ?>

	</div>
	<!-- #tribe-events-header -->
	<?php do_action( 'tribe_events_after_header' ) ?>

	<!-- Month Grid -->
	<?php tribe_get_template_part( 'month/loop', 'grid' ) ?>

	<!-- Month Footer -->
	<?php do_action( 'tribe_events_before_footer' ) ?>
	<div id="tribe-events-footer">

		<!-- Footer Navigation -->
		<?php do_action( 'tribe_events_before_footer_nav' ); ?>
		<?php tribe_get_template_part( 'month/nav' ); ?>
		<?php do_action( 'tribe_events_after_footer_nav' ); ?>

	</div>
	<!-- #tribe-events-footer -->
	<?php do_action( 'tribe_events_after_footer' ) ?>

	<?php tribe_get_template_part( 'month/mobile' ); ?>
	<?php tribe_get_template_part( 'month/tooltip' ); ?>

</div><!-- #tribe-events-content -->
