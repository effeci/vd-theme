<?php
/**
 * Velostazione Dynamo (VD) Theme
 *
 * This file adds functions to the VD Theme.
 *
 * @package Velostazione theme
 * @author  El Jefe
 * @license GPL-2.0+
 * @link    https://www.linkedin.com/in/filippocarnevali/
 */

// Start the engine.
include_once( get_template_directory() . '/lib/init.php' );

// Setup Theme.
include_once( get_stylesheet_directory() . '/lib/theme-defaults.php' );

// Set Localization (do not remove).
add_action( 'after_setup_theme', 'genesis_sample_localization_setup' );
function genesis_sample_localization_setup(){
	load_child_theme_textdomain( 'genesis-sample', get_stylesheet_directory() . '/languages' );
}

// Add the helper functions.
include_once( get_stylesheet_directory() . '/lib/helper-functions.php' );

// Add Image upload and Color select to WordPress Theme Customizer.
require_once( get_stylesheet_directory() . '/lib/customize.php' );

// Include Customizer CSS.
include_once( get_stylesheet_directory() . '/lib/output.php' );

// Add WooCommerce support.
include_once( get_stylesheet_directory() . '/lib/woocommerce/woocommerce-setup.php' );

// Add the required WooCommerce styles and Customizer CSS.
include_once( get_stylesheet_directory() . '/lib/woocommerce/woocommerce-output.php' );

// Add the Genesis Connect WooCommerce notice.
include_once( get_stylesheet_directory() . '/lib/woocommerce/woocommerce-notice.php' );

// Child theme (do not remove).
define( 'CHILD_THEME_NAME', 'Genesis Sample' );
define( 'CHILD_THEME_URL', 'http://www.studiopress.com/' );
define( 'CHILD_THEME_VERSION', '2.3.0' );

// Enqueue Scripts and Styles.
add_action( 'wp_enqueue_scripts', 'genesis_sample_enqueue_scripts_styles' );
function genesis_sample_enqueue_scripts_styles() {

	wp_enqueue_style( 'genesis-sample-fonts', '//fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700', array(), CHILD_THEME_VERSION );
	// Custom Font
	wp_enqueue_style( 'Titillium', '//fonts.googleapis.com/css?family=Titillium+Web:400,700', array(), CHILD_THEME_VERSION );
	// --
	wp_enqueue_style( 'dashicons' );
 	// Custom CSS
 	//wp_enqueue_style ('shop-menu',  get_stylesheet_directory_uri() . '/shop-menu.css' );
 	// --
	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
	wp_enqueue_script( 'genesis-sample-responsive-menu', get_stylesheet_directory_uri() . "/js/responsive-menus{$suffix}.js", array( 'jquery' ), CHILD_THEME_VERSION, true );
	// Custom JS
	wp_enqueue_script( 'jf_custom_code', get_stylesheet_directory_uri() . "/js/jefe_js.js", array( 'jquery' ), CHILD_THEME_VERSION, true );
	// --
	wp_localize_script(
		'genesis-sample-responsive-menu',
		'genesis_responsive_menu',
		genesis_sample_responsive_menu_settings()
	);

}
//*******************************************
//*******************************************
// WP HEAD CLEANING - START 
//*******************************************
//*******************************************
// Impedisce a WP di aggiungere i <p></p>
	//	remove_filter( 'the_content', 'wpautop' );
	//	remove_filter( 'the_excerpt', 'wpautop' );
	// Rimuove il meta "generator"
	//	remove_action('wp_head', 'wp_generator');
	// Rimuovi le emoji nell'header 
	//remove_action( 'wp_head', 'print_emoji_detection_script', 7 );remove_action( 'wp_print_styles', 'print_emoji_styles' ); remove_action( 'admin_print_scripts', 'print_emoji_detection_script' ); remove_action( 'admin_print_styles', 'print_emoji_styles' );
// Remove auto generated feed links
//function my_remove_feeds() {remove_action( 'wp_head', 'feed_links_extra', 3 ); remove_action( 'wp_head', 'feed_links', 2 );	} add_action( 'after_setup_theme', 'my_remove_feeds' ); remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
	/**
	 * Remove the Tribe Customier css <script>
	 */
		function tribe_remove_customizer_css(){if ( class_exists( 'Tribe__Customizer' ) ) {remove_action( 'wp_print_footer_scripts', array( Tribe__Customizer::instance(), 'print_css_template' ), 15 );}}add_action( 'wp_footer', 'tribe_remove_customizer_css' );
//*******************************************
//*******************************************
//WP HEAD CLEANING - END
//*******************************************
//*******************************************

// Define our responsive menu settings.
function genesis_sample_responsive_menu_settings() {

	$settings = array(
		'mainMenu'          => __( 'Menu', 'genesis-sample' ),
		'menuIconClass'     => 'dashicons-before dashicons-menu',
		'subMenu'           => __( 'Submenu', 'genesis-sample' ),
		'subMenuIconsClass' => 'dashicons-before dashicons-arrow-down-alt2',
		'menuClasses'       => array(
			'combine' => array(
				'.nav-primary',
				'.nav-header',
			),
			'others'  => array(),
		),
	);

	return $settings;

}

// Add HTML5 markup structure.
add_theme_support( 'html5', array( 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ) );

// Add Accessibility support.
add_theme_support( 'genesis-accessibility', array( '404-page', 'drop-down-menu', 'headings', 'rems', 'search-form', 'skip-links' ) );

// Add viewport meta tag for mobile browsers.
add_theme_support( 'genesis-responsive-viewport' );

// Add support for custom header.
add_theme_support( 'custom-header', array(
	'width'           => 600,
	'height'          => 160,
	'header-selector' => '.site-title a',
	'header-text'     => false,
	'flex-height'     => true,
) );

// Add support for custom background.
add_theme_support( 'custom-background' );

// Add support for after entry widget.
add_theme_support( 'genesis-after-entry-widget-area' );

// Add support for 3-column footer widgets.
add_theme_support( 'genesis-footer-widgets', 2 );

// Add Image Sizes.
add_image_size( 'featured-image', 720, 400, TRUE );

// Rename primary and secondary navigation menus.
add_theme_support( 'genesis-menus', array( 'primary' => __( 'After Header Menu', 'genesis-sample' ), 'secondary' => __( 'Footer Menu', 'genesis-sample' ) ) );

// Reposition the secondary navigation menu.
remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action( 'genesis_footer', 'genesis_do_subnav', 5 );

// Reduce the secondary navigation menu to one level depth.
add_filter( 'wp_nav_menu_args', 'genesis_sample_secondary_menu_args' );
function genesis_sample_secondary_menu_args( $args ) {if ( 'secondary' != $args['theme_location'] ) {return $args;}$args['depth'] = 1;return $args;}

// Modify size of the Gravatar in the author box.
add_filter( 'genesis_author_box_gravatar_size', 'genesis_sample_author_box_gravatar' );
function genesis_sample_author_box_gravatar( $size ) {return 90;}

// Modify size of the Gravatar in the entry comments.
add_filter( 'genesis_comment_list_args', 'genesis_sample_comments_gravatar' );
function genesis_sample_comments_gravatar( $args ) {$args['avatar_size'] = 60;return $args;}


// Cancella il titolo in Home (WP MAIN)
// add_action( 'get_header', 'jf_home_title_removal' );
// function jf_home_title_removal () { 
// 	if (is_page(array(3088,9466,9449,6484,4989,4887,7932,7556,10052,10032,9987,9459,10462,9712,2010,9975,10210,10271,10268,9827,130))) {
// 		remove_action( 'genesis_entry_header' , 'genesis_do_post_title' );
// 	}
// }
// Cancella il titolo in Home (WP EN)
// add_action( 'get_header', 'jf_home_title_removal' );
// function jf_home_title_removal () { 
// 	if (is_page(array(101,201,173,116,128,126,213))) {
// 		remove_action( 'genesis_entry_header' , 'genesis_do_post_title' );
// 	}
// }

//* Remove .site-inner
add_filter( 'genesis_markup_site-inner', '__return_null' );
//************************************** 
//************************************** 
//* Cambiare il testo nel footer

add_filter('genesis_footer_creds_text', 'jefe_footer_creds_filter');
function jefe_footer_creds_filter( $creds ) {
	$creds = '[footer_copyright] &middot; Dynamo Soc. Cooperativa a R.L. - P.Iva/CF: 03482461203 - Capitale sociale: 17.500 euro.';
	return $creds;
}
/**** Using Custom Logo with Genesis via Customizer
Raw
***/

add_theme_support( 'custom-logo', array(
	'height'      => 240, // set to your dimensions
	'width'       => 240,
	'flex-height' => true,
	'flex-width'  => true,
) );


/**
 * Add an image inline in the site title element for the main logo
 *
 * The custom logo is then added via the Customiser
 *
 * @param string $title All the mark up title.
 * @param string $inside Mark up inside the title.
 * @param string $wrap Mark up on the title.
 * @author @_AlphaBlossom
 * @author @_neilgee
 */
function genesischild_custom_logo( $title, $inside, $wrap ) {
	// Check to see if the Custom Logo function exists and set what goes inside the wrapping tags.
	if ( function_exists( 'has_custom_logo' ) && has_custom_logo() ) :
		$logo = get_custom_logo();
	else :
	 	$logo = get_bloginfo( 'name' );
	endif;
 	 // Use this wrap if no custom logo - wrap around the site name
	 $inside = sprintf( '<a href="%s" title="%s">%s</a>', trailingslashit( home_url() ), esc_attr( get_bloginfo( 'name' ) ), $logo );
	 // Determine which wrapping tags to use - changed is_home to is_front_page to fix Genesis bug.
	 $wrap = is_front_page() && 'title' === genesis_get_seo_option( 'home_h1_on' ) ? 'h1' : 'p';
	 // A little fallback, in case an SEO plugin is active - changed is_home to is_front_page to fix Genesis bug.
	 $wrap = is_front_page() && ! genesis_get_seo_option( 'home_h1_on' ) ? 'h1' : $wrap;
	 // And finally, $wrap in h1 if HTML5 & semantic headings enabled.
	 $wrap = genesis_html5() && genesis_get_seo_option( 'semantic_headings' ) ? 'h1' : $wrap;
	 $title = sprintf( '<%1$s %2$s>%3$s</%1$s>', $wrap, genesis_attr( 'site-title' ), $inside );
	 return $title;
}
add_filter( 'genesis_seo_title','genesischild_custom_logo', 10, 3 );
/**
 * Add class for screen readers to site description.
 * This will keep the site description mark up but will not have any visual presence on the page
 * This runs if their is a header image set in the Customiser.
 *
 * @param string $attributes Add screen reader class if custom logo is set.
 *
 * @author @_neilgee
 */
 function genesischild_add_site_description_class( $attributes ) {
	if ( function_exists( 'has_custom_logo' ) && has_custom_logo() ) {
		$attributes['class'] .= ' screen-reader-text';
		return $attributes;
	}
	else {
		return $attributes;
	}
 }
 add_filter( 'genesis_attr_site-description', 'genesischild_add_site_description_class' );


 /*
* Genesis Layout of The Event Calendar Views for all Templates
* The Events Calendar @3.10
* Genesis @2.1.2
* Options - full-width-content, content-sidebar, sidebar-content, content-sidebar-sidebar, sidebar-sidebar-content, sidebar-content-sidebar
*/
//Target all Event Views (Month, List, Map etc), Single Events, Single Venues, and Single Organizers
add_filter( 'genesis_pre_get_option_site_layout', 'tribe_genesis_all_layouts' );
function tribe_genesis_all_layouts() {
    if( class_exists( 'Tribe__Events__Main' ) && tribe_is_event_query() ) {
        return 'full-width-content';}}
add_filter( 'edit_post_link', '__return_false' );
/*
*
* TOP BAR - RICERCA E ENG
* top_block - div contenitore
* top_block_item - div che contiene i singoli elementi 
*
*/
function jefe_top_block () { 
?> 
<div class="top_block">
<div class="top_block_item"><a href="#topsearch" style="" class="popup-with-form " id="top_search_icon"><img src="/wp-content/themes/vd-theme/images/searchicon2x.png" style="height: 18px !important;"></a>
	<div class="white-popup-block zoom-anim-dialog mfp-hide" id="topsearch"> 
		<?php get_search_form(); ?>
	</div></div>
<div class=" top_block_item"><a href="https://dynamo.bo.it/en/"><img src="/wp-content/uploads/2017/02/ENG.png"  style="height: 18px !important;"></a></div></div>
	<?php }
	add_action('genesis_header_right', 'jefe_top_block');

/*
*
* RIMUOVI IL TITOLO SE IL CAMPO 
* jefe_title non è vuoto
* Nei campi custom della pagina basta impostare un qualsiasi valore. 
*
*/
// add_action( 'get_header', 'jefe_title_removal_conditional');
// function jefe_title_removal__conditional (){
// 	$jefe_v_title = get_post_meta( get_the_id(), 'jefe_title', true);
// 	if ( ! empty( $jefe_v_title ) ) {
// 		remove_action( 'genesis_entry_header' , 'genesis_do_post_title' );
// 	}
// }

/*
*
* RIMUOVI IL TITOLO SE è UNA PAGINA
*
*/

add_action( 'get_header', 'jefe_page_title_removal');
function jefe_page_title_removal(){
if (is_singular( 'page' )) {remove_action( 'genesis_entry_header' , 'genesis_do_post_title' );
	}
}

