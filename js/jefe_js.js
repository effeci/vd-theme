jQuery(function( $ ){

	// Add shrink class to site header after 50px
	$(document).on("scroll", function(){

		if($(document).scrollTop() > 50){
			$(".site-header").addClass("shrink");	
			$(".custom-logo").addClass("shrink-logo");	
			$(".genesis-nav-menu a").addClass("shrink-menu");	

		} else {
			$(".site-header").removeClass("shrink");			
			$(".custom-logo").removeClass("shrink-logo");	
			$(".genesis-nav-menu a").removeClass("shrink-menu");	
		}
		
	});


});